---
title: "Model building"
author: "Big Data Innovation Hub"
date: "January 16, 2019"
output: html_document
---

```{r}
library(tidyverse)
library(mlr)
library(mlrCPO)
```

### Import relevant datasets
```{r}
tr_val_df <- readRDS("../../data/train_validation.rds")
test_df <- readRDS("../../data/test.rds")
```

### Create the relevant mlr objects
```{r}
pmts_task = tr_val_df %>%
  #Remove non usable features in model building
  select(-step, -isFlaggedFraud, -nameDest, -nameOrig) %>%
  #Apply log transform to relevant variables
  mutate_at(vars(oldbalanceOrg, oldbalanceDest, newbalanceOrig,newbalanceDest,
                 amount, diff_balanceOrig, diff_balanceDest,dest_bal_dist), 
            function(col) sign(col)*log(abs(col) + 1)) %>%
  #Convert logical attribute to integer
  mutate_if(is.logical,as.integer) %>%
  
  #create the task
  makeClassifTask(data = .,
                  target = "isFraud",
                  positive = "1")
```

#### Learners
##### Random forest
With only the orignal features of the dataset
```{r}
rf_l <- cpoScale()%>>%
  cpoUndersample(rate = 0.2) %>>%
  cpoSelect(index = 1:6) %>>%
  makeLearner(cl = "classif.randomForest",predict.type = "prob",
              sampsize = c(1000,840))
```

##### Logistic Regression
With only the `maximum_transfer` feature
```{r}
logreg_l = cpoScale()%>>%
  cpoUndersample(rate = 0.2) %>>%
  cpoSelect(names = "maximum_transfer") %>>%

  (makeLearner("classif.logreg", predict.type = "prob")%>%
  makeWeightedClassesWrapper(wcw.weight = 60))
```

With all features
```{r}
all_f_logreg_l = cpoScale()%>>%
  cpoUndersample(rate = 0.2) %>>%

  (makeLearner("classif.logreg", predict.type = "prob")%>%
  makeWeightedClassesWrapper(wcw.weight = 60))
```


##### Decision Tree (C50)
With only the `maximum_transfer` feature
```{r}
c50_l = cpoSelect(names = "maximum_transfer") %>>%
  cpoUndersample(rate = 0.2) %>>%

  (makeLearner("classif.C50", predict.type = "prob") %>%
  makeWeightedClassesWrapper(wcw.weight = 60))
```

With all features
```{r}
all_f_c50_l = cpoUndersample(rate = 0.2) %>>%

  (makeLearner("classif.C50", predict.type = "prob",
               par.vals = list(earlyStopping=FALSE)) %>%
  makeWeightedClassesWrapper(wcw.weight = 60))
```


#### Measurements
```{r}
perf_meas = list(auc,logloss,bac)
```


### Train Models
####Define train and validation indices
```{r}
set.seed(123)

split = 0.7

training_index = sample(seq_len(getTaskSize(pmts_task)),
                        size = floor(getTaskSize(pmts_task)*split))

validation_index = setdiff(seq_len(getTaskSize(pmts_task)),training_index) 
```

#### Random Forest
```{r}
rf_model <- train(learner = rf_l,
                  task = pmts_task,
                  subset = training_index)
```

Predictions of the model
```{r}
train_rf_pred <- predict(rf_model,
                         task = pmts_task,
                         subset = training_index) 

val_rf_pred <- predict(rf_model,
                       task = pmts_task,
                       subset = validation_index)
```

Performance of the model
```{r}
#Train
performance(train_rf_pred, measures = perf_meas)
caret::confusionMatrix(as.factor(train_rf_pred$data$response),
                       as.factor(train_rf_pred$data$truth),
                       positive = "1")
```

```{r}
#Test
performance(val_rf_pred, measures = perf_meas)
caret::confusionMatrix(val_rf_pred$data$response,
                       val_rf_pred$data$truth,
                       positive = "1")
```

Since we are going to do the same sequence for the rest of the models, let's code this sequence into a function
```{r}
evaluate_model <- function(learner,task,train_index, validation_index, perf_meas){
  #Train model
  model <- train(learner, task, subset = training_index)
  
  #Create predictions
  train_pred <- predict(model, task, subset = training_index) 
  val_pred <- predict(model, task, subset = validation_index)
  
  #Performance of train
  print("Performance in train set")
  print(performance(train_pred, measures = perf_meas))
  print(caret::confusionMatrix(as.factor(train_pred$data$response),
                               as.factor(train_pred$data$truth),
                               positive = "1"))

  #Performance of test
  print("Performance in test set")
  print(performance(val_pred, measures = perf_meas))
  print(caret::confusionMatrix(val_pred$data$response,
                               val_pred$data$truth,
                               positive = "1"))
  
  #Return the model and predictions
  results_eval = list(model= model, train_pred=train_pred, val_pred=val_pred)
  return(results_eval)
}
```

```{r}
rf_results <- evaluate_model(learner = rf_l,task = pmts_task,
                             train_index = training_index, 
                             validation_index = validation_index,
                             perf_meas = perf_meas)
```


#### Regresión Logística con un solo feature
```{r}
logreg_results <- evaluate_model(learner = logreg_l,task = pmts_task,
                                 train_index = training_index,
                                 validation_index = validation_index,
                                 perf_meas = perf_meas)
```


#### Decision Trees con un solo feature
```{r}
c50_results <- evaluate_model(learner = c50_l, task = pmts_task,
                              train_index = training_index,
                              validation_index = validation_index,
                              perf_meas = perf_meas)
```

#### Decision Tree con múltiples features
```{r}
allf_c50_results <- evaluate_model(learner = all_f_c50_l, task = pmts_task,
                                   train_index = training_index,
                                   validation_index = validation_index,
                                   perf_meas = perf_meas)
```


```{r}
c50_model = allf_c50_results$model %>%
  getLearnerModel() %>% 
  getLearnerModel() %>% 
  getLearnerModel()

summary(c50_model)
```

#### Regresión lineal con mútiples features
```{r}
allf_lg_results <- evaluate_model(learner = all_f_logreg_l, task = pmts_task,
                                  train_index = training_index,
                                  validation_index = validation_index,
                                  perf_meas = perf_meas)
```

```{r}
logreg_model = allf_lg_results$model %>%
  getLearnerModel() %>% 
  getLearnerModel() %>% 
  getLearnerModel()

summary(logreg_model)

```

```{r}
plot(logreg_model)
```

